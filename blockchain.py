from hashlib import sha256
from time import time
import pickle
from json import dumps

class Blockchain:
    def __init__(self):
        self.chain = [Block(str(int(time())))]
        self.current_transactions = []
        self.difficulty = 5
        self.blockTime = 20

    def get_last_block(self):
        return self.chain[len(self.chain) - 1]

    def add_block(self, block):
        block.prev_hash = self.get_last_block().hash
        block.hash = block.get_hash()
        block.mine(self.difficulty)
        self.chain.append(block)
        self.current_transactions = []

        #self.difficulty += (-1, 1)[int(time()) - int(self.get_last_block().timestamp) < self.blockTime]

    def new_transaction(self, sender, recipient, amount):
        self.current_transactions.append({
            'sender': sender,
            'recipient': recipient,
            'amount': amount
        })
        return self.get_last_block()

    #Проверка блокчейна
    def is_valid(self):
        for i in range(1, len(self.chain)):
            current_block = self.chain[i]
            prev_block = self.chain[i -1]
            if current_block.hash != current_block.get_hash() or prev_block.hash != current_block.prev_hash:
                return False

    def save(self):
        with open("chain.pkl", "wb") as fp:
            pickle.dump(self, fp)


    def __getstate__(self) -> dict:
        state = {}
        state['chain'] = self.chain
        state['current_transactions'] = self.current_transactions
        state['difficulty'] = self.difficulty
        state['blockTime'] = self.blockTime
        return state

    def __setstate__(self, state: dict):
        self.chain = state['chain']
        self.current_transactions = state['current_transactions']
        self.difficulty = state['difficulty']
        self.blockTime = state['blockTime']

    def __repr__(self):
        return dumps([{'data': item.data, 'timestamp': item.timestamp, 'nonce': item.nonce, 'PrevHash': item.prev_hash} for item in self.chain], indent=4)


class Block:
    def __init__(self, timestamp=None, data=None):
        self.timestamp = timestamp or int(time())
        self.data = [] if data is None else data
        self.prev_hash = None
        self.nonce = 0
        self.hash = self.get_hash()

    def get_hash(self):
        block_hash = sha256()
        block_hash.update(str(self.prev_hash).encode('utf-8'))
        block_hash.update(str(self.timestamp).encode('utf-8'))
        block_hash.update(str(self.data).encode('utf-8'))
        block_hash.update(str(self.nonce).encode('utf-8'))
        return block_hash.hexdigest()

    def mine(self, difficulty):
        while self.hash[:difficulty] != '0' * difficulty:
            self.nonce += 1
            self.hash = self.get_hash()

