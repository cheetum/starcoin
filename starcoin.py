from app import app
from blockchain import Blockchain
from pickle import load

with open("chain.pkl", "rb") as fp:
    starchain = load(fp)
if starchain.is_valid() is False:
    print('Error chain')
    starchain = Blockchain()

if __name__ == "__main__":
    app.run(debug=True)