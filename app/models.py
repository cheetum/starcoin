from app import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from datetime import datetime
from hashlib import sha256


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    registration_date = db.Column(db.DateTime, default=datetime.now)
    last_seen = db.Column(db.DateTime, default=datetime.now)
    wallet = db.Column(db.Integer, db.ForeignKey('bank_account.wallet'))
    author = db.relationship('BankAccount', uselist=False)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


    def __repr__(self):
        return self.username


class BankAccount(db.Model):

    wallet = db.Column(db.String(128), primary_key=True)
    amount = db.Column(db.Integer)

    def set_wallet(self, username):
        self.wallet = sha256(username.encode('utf-8')).hexdigest()

#Получение данных прользователя по его идентификатору сеанса Flask
@login.user_loader
def load_user(id):
    return User.query.get(int(id))

