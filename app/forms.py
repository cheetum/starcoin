from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField
from wtforms.validators import DataRequired, EqualTo, ValidationError, Email, Length
from app.models import User, BankAccount
from flask_login import current_user

class LoginForm(FlaskForm):
    username = StringField('Логин', validators=[DataRequired()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    remember_me = BooleanField('Запомнить меня')
    submit = SubmitField('Войти')

class RegistrationForm(FlaskForm):
    username = StringField('Логин', validators=[DataRequired()])
    email = StringField('е-мейл', validators=[DataRequired(), Email()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    password2 = PasswordField('Пароль еще раз', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Создать аккаунт')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Данное имя занято. Попробуйте другое')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Уже есть аккаунт с данной почтой. Выберите другую')


class TransactionForm(FlaskForm):
    recipient = StringField('Кому (Логин):', validators=[DataRequired()])
    amount = IntegerField('Сколько', validators=[DataRequired()])
    submit = SubmitField('Отправить')

    def validate_recipient(self, recipient):
        user = User.query.filter_by(username=recipient.data).first()
        if user is None:
            raise ValidationError('Такой пользователь не найден')

    def validate_amount(self, amount):
        balance = BankAccount.query.filter_by(wallet=current_user.wallet).first().amount
        if amount.data > balance:
            raise ValidationError('Недостаточно средств')