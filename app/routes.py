from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required

from blockchain import Block
from starcoin import starchain
from app import app, db
from app.forms import LoginForm, RegistrationForm, TransactionForm
from app.models import User, BankAccount
from werkzeug.urls import url_parse
from datetime import datetime
from time import time

@app.route('/')
@app.route('/index')
@login_required
def index():
    return render_template('index.html')

@app.route('/about_us')
def about_us():
    return render_template('about_us.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit(): #ННПНОИ
        user = User.query.filter_by(username=form.username.data).first() #ННПНОИ
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        bank_account = BankAccount(amount=100)
        bank_account.set_wallet(form.username.data)
        user = User(username=form.username.data, email=form.email.data, registration_date=datetime.now(), wallet=bank_account.wallet)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.add(bank_account)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@app.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first()
    money = BankAccount.query.filter_by(wallet=user.wallet).first().amount
    return render_template('users.html', user=user, amount=money)


@app.route('/mine', methods=['GET'])
def mine():
    for i in range(1):
        block = Block(data=starchain.current_transactions)
        block.mine(starchain.difficulty)
        starchain.add_block(block)
        print(int(time()) - int(starchain.get_last_block().timestamp))
        starchain.new_transaction(0, current_user.wallet, 1)
        starchain.save()
        account = BankAccount.query.filter_by(wallet=current_user.wallet).first()
        account.amount += 1
        db.session.commit()
    return redirect(url_for('chain'))


@app.route('/transactions/<username>', methods=['GET','POST'])
@login_required
def new_transaction(username):
    form = TransactionForm()
    if form.validate_on_submit():
        sender = User.query.filter_by(username=username).first()
        recipient = User.query.filter_by(username=form.recipient.data).first()
        sender = BankAccount.query.filter_by(wallet=sender.wallet).first()
        recipient = BankAccount.query.filter_by(wallet=recipient.wallet).first()
        starchain.new_transaction(sender.wallet, recipient.wallet, form.amount.data)
        starchain.save()
        sender.amount -= form.amount.data
        recipient.amount += form.amount.data
        db.session.commit()
        flash('Отправлено')
        return redirect(url_for('user', username=username))
    return render_template('new_transaction.html', username=username, form=form)


@app.route('/chain', methods=['GET'])
@login_required
def chain():
    return render_template('chain.html', blockchain=starchain.chain)

@app.route('/reset_chain', methods=['GET'])
@login_required
def reset_chain():
    starchain.__init__()
    starchain.save()
    flash('Цепь перезапущена')
    return redirect(url_for('chain'))

@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()